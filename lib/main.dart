import 'package:flutter/material.dart';
import 'package:state/screens/date_screen.dart';
import 'package:state/screens/alert_screen.dart';
import 'package:state/screens/switch_screen.dart';
import 'package:state/screens/slider_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var urlA = 200;
  var urlB = 200;
  var urlC = 200;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            title: const Text('APIs for Images'),
            bottom: const TabBar(
              tabs: [
                Tab(
                  icon: Icon(Icons.calendar_view_month),
                  text: "Date",
                ),
                Tab(
                  icon: Icon(Icons.warning),
                  text: "Alert",
                ),
                Tab(
                  icon: Icon(Icons.touch_app),
                  text: "Switch",
                ),
                Tab(
                  icon: Icon(Icons.bar_chart),
                  text: "Slider",
                ),
              ],
            ),
          ),
          body: const TabBarView(
            children: [
              Padding(
                padding: EdgeInsets.all(20),
                child: DateWidget(),
              ),
              Padding(
                padding: EdgeInsets.all(20),
                child: AlertWidget(),
              ),
              Padding(
                padding: EdgeInsets.all(20),
                child: SwitchWidget(),
              ),
              Padding(
                padding: EdgeInsets.all(20),
                child: SliderWidget(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
