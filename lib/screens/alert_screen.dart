import 'package:flutter/material.dart';

class AlertWidget extends StatelessWidget {
  const AlertWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      ElevatedButton(
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: const Text("To state or not state"),
                  content: const Text(
                      "I understand i should never use a StatefulWidget when a StatelessWidget ca do the job"),
                  actions: [
                    ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text("YES!"))
                  ],
                );
              });
        },
        child: const Text("Show warning"),
      ),
    ]);
  }
}
