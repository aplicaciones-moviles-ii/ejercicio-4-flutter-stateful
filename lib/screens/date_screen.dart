import 'package:flutter/material.dart';

class DateWidget extends StatefulWidget {
  const DateWidget({Key? key}) : super(key: key);

  @override
  _DateWidgetState createState() => _DateWidgetState();
}

class _DateWidgetState extends State<DateWidget> {
  @override
  Widget build(BuildContext context) {
    var date = DateTime.now();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Center(
          child: Text(
            date.year.toString() +
                "-" +
                date.month.toString() +
                "-" +
                date.day.toString(),
            style: const TextStyle(fontSize: 25.0),
          ),
        ),
        Center(
          child: Text(
            date.hour.toString() +
                ":" +
                date.minute.toString() +
                ":" +
                date.second.toString() +
                "." +
                date.millisecond.toString(),
            style: const TextStyle(fontSize: 25.0),
          ),
        ),
        ElevatedButton(
            onPressed: () {
              setState(() {
                date = DateTime.now();
              });
            },
            child: const Text("Update date"))
      ],
    );
  }
}
