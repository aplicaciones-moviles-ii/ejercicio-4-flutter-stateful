import 'package:flutter/material.dart';

class SliderWidget extends StatefulWidget {
  const SliderWidget({Key? key}) : super(key: key);

  @override
  _SliderWidgetState createState() => _SliderWidgetState();
}

class _SliderWidgetState extends State<SliderWidget> {
  double valor = 42;
  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Center(
            child: Text(
              valor.toInt().toString(),
              style: const TextStyle(fontSize: 25.0),
            ),
          ),
          Slider(
              min: 0,
              max: 100,
              value: valor,
              onChanged: (newRating) {
                setState(() {
                  valor = newRating;
                });
              })
        ]);
  }
}
