import 'package:flutter/material.dart';

class SwitchWidget extends StatefulWidget {
  const SwitchWidget({Key? key}) : super(key: key);

  @override
  _SwitchWidgetState createState() => _SwitchWidgetState();
}

class _SwitchWidgetState extends State<SwitchWidget> {
  bool state = false;
  String estado = "OFF";
  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Center(
            child: Text(
              estado,
              style: const TextStyle(fontSize: 25.0),
            ),
          ),
          Switch(
              value: state,
              onChanged: (bool s) {
                setState(() {
                  state = s;
                  estado = s == true ? "ON" : "OFF";
                });
              })
        ]);
  }
}
